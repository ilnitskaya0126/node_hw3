const { response } = require('express');
const mongoose = require('mongoose');
const { Note } = require('./loadSchema.js');

const userSchema = mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      uniq: true
    },
    date: {
      type: Date,
      default: Date.now()
    },
    password: {
      type: String,
      required: true
    },
    role: {
      type: String,
      required: true
    },

  },
  {
    collection: 'users'
  }
);

userSchema.virtual('notes', {
  ref: 'Note',
  localField: '_id',
  foreignField: 'userId',
});

userSchema.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();

  delete userObject.password;
  delete userObject.__v;
  userObject._id.toString();

  return userObject;
};

userSchema.pre('remove', async function (next) {
  const user = this;
  await Note.deleteMany({ userId: user.id });
  next();
});

module.exports.User = mongoose.model('User', userSchema);
