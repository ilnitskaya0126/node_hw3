const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema(
  {

    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'User'
    },
    assigned_to: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    type: {
      type: String,
      enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
      required: true,
      default: 'SPRINTER',
    },
    status: {
      type: String,
      enum: ['OL', 'IS'],
      default: 'IS',
    },
    created_date: {
      type: Date,
      default: Date.now()
    },
  },
  {
    collection: 'truck'
  }
);

truckSchema.methods.toJSON = function () {
  const truck = this;
  const truckObject = truck.toObject();

  delete truckObject.__v;
  truckObject._id.toString();

  return truckObject;
};

module.exports.Truck = mongoose.model('Truck', truckSchema);
