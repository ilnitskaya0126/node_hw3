const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true
    },
    payload: {
      type: Number,
      required: true,
    },
    pickup_address: {
      type: String,
      required: true,
    },
    delivery_address: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: ["NEW", "POSTED", "ASSIGNED", "SHIPPED"],
      default: "NEW",
    },
    state: {
      type: String,
      enum: ["En route to Pick Up", "Arrived to Pick Up", "En route to delivery", "Arrived to delivery"],
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'User'
    },
    assigned_to: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    dimensions: {
      width : {
        type: String,
        required: true,
      },
      length : {
        type: String,
        required: true,
      },
      height : {
        type: String,
        required: true,
      },
    },
    logs: [{
      message: {
        type: String,
      },
      time: {
        type: Date,
        default: Date.now(),
      },
    }],
    created_date: {
      type: Date,
      default: Date.now(),
    },
  },
  {
    collection: 'loades'
  }
);

loadSchema.methods.toJSON = function () {
  const load = this;
  const loadObject = load.toObject();

  delete loadObject.__v;
  loadObject._id.toString();

  return loadObject;
};

module.exports.Load = mongoose.model('Load', loadSchema);
