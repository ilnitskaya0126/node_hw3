const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const loadRouter = require('./routers/loadRouter');
const truckRouter = require('./routers/truckRouter');


app.use(express.json());
//app.use(express.static('build'));
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/loads', loadRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use((err, req, res, next) => {
  res.status(500).json({ message: err.message });
});

const start = async () => {
  try {
    await mongoose.connect(
      'mongodb+srv://testuser:testuser@cluster0.lrssg.mongodb.net/test?retryWrites=true&w=majority',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
      }
    );
  } catch (err) {
    console.log(err.message);
  }

  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();
