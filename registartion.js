const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('./config.js');
const { User } = require('./schema/userSchema.js');

module.exports.registration = async (req, res) => {
  const { email, password, role} = req.body;

  if(!email || !password || !role) {
    return res.status(400).json({ message: 'Enter your password, login role' });
  }

  let user = await User.find({ email });
  console.log(user);

  if(user.length !== 0) {
    return res.status(400).json({ message: 'User is already exist!' });
  }

  user = new User({
    email: email,
    password: await bcrypt.hash(password, 10),
    role: role,
  });

  const token = jwt.sign({ email: user.email, _id: user._id },JWT_SECRET);

  await user.save();

  return res.status(200).json({ message: 'Success' });
};

module.exports.login = async (req, res) => {
  const { email, password} = req.body;

  if(!email || !password) {
    return res.status(400).json({ message: 'Enter correct password and email' });
  }

  const user = await User.findOne({ email });

  if (!user || !(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({ message: `Incorrect email or password, please try again` });
  }

  const token = jwt.sign({ email: user.email, _id: user._id },JWT_SECRET);
  
  res.status(200).json({ message: 'Success', jwt_token: token });
};
