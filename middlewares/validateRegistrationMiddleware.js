const Joi = require('joi');

module.exports.validateRegistrationMiddleware = async (req, res, next) => {
  const schema = Joi.object({
    email:  Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }).required(),
    password: Joi.string().pattern(/^[a-zA-Z0-9]{6,30}$/).required(),
    role :Joi.string().pattern(/^SHIPPER|DRIVER$/).required(),
  });

  const { error, value } = schema.validate(req.body);
  const valid = error == null;
  if (!valid) {
    res.status(400).json({message: 'Invalid data'});
  }
  next();
};
