const express = require('express');
const router = express.Router();
const {validateRegistrationMiddleware} = require('../middlewares/validateRegistrationMiddleware');
const { registration, login } = require('../registartion.js');


router.post('/register',(req, res, next) => validateRegistrationMiddleware(req, res, next).catch(next),(req, res, next) => registration(req, res, next).catch(next));
router.post('/login', (req, res, next) => login(req, res, next).catch(next));

module.exports = router;
