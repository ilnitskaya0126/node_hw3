const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const { Truck } = require('../schema/truckSchema.js');

router.get('/', authMiddleware, async (req, res) => {
  const user = req.user;

  if (user.role !== "DRIVER") {
    return res.status(403).json({ message: 'Not available for this role' });
  }

  const truck = await Truck.find({created_by: user._id});

  if (truck.length == 0) {
    return res.status(404).json({ message: 'No such truck' });
  }

  return res.status(200).send({trucks:truck});

});

router.post('/', authMiddleware, async (req, res) => {
  const user = req.user;

  if (user.role !== "DRIVER") {
    return res.status(403).json({ message: 'Not available for this role' });
  }

  const truck = new Truck({
    created_by: req.user._id,
    type: req.body.type,
  });

  try {
    await truck.save();
    return res.status(200).json({ message: "Truck created successfully" });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }

});

router.get('/:id', authMiddleware, async (req, res) => {
  const user = req.user;

  if (user.role !== "DRIVER") {
    return res.status(403).json({ message: 'Not available for this role' });
  }

  let _id = req.params.id;
  const truck = await Truck.findOne({ _id});

  if (!truck) {
    return res.status(400).json({ message: 'No such truck' });
  }

  return res.status(200).send(truck);
});

router.put('/:id', authMiddleware, async (req, res) => {
  const user = req.user;

  if (user.role !== "DRIVER") {
    return res.status(403).json({ message: 'Not available for this role' });
  }

  let _id = req.params.id;
  let truck = await Truck.findOneAndUpdate({_id}, req.body, {new: true,upsert: true });

  if (!truck) {
    return res.status(400).json({ message: 'No such note' });
  }

  return res.status(200).json({ message: 'Truck details changed successfully' });

});


router.delete('/:id', authMiddleware, async (req, res) => {
  const user = req.user;

  if (user.role !== "DRIVER") {
    return res.status(403).json({ message: 'Not available for this role' });
  }

  let _id = req.params.id;

  const truck = await Truck.findOne({ _id});

  if (!truck) {
    return res.status(400).json({ message: 'No such load' });
  }

  await Truck.findOneAndDelete({_id});
  return res.status(200).json({ message: 'Truck deleted successfully' });

});

router.post('/:id/assign', authMiddleware, async (req, res) => {
  const user = req.user;

  if (user.role !== "DRIVER") {
    return res.status(403).json({ message: 'Not available for this role' });
  }

  const _id = req.params.id;

  const truck = await Truck.findOne({_id, created_by: user._id});

  if (!truck) {
    return res.status(404).json({ message: 'No such truck' });
  }

  truck.assigned_to = user._id;
  truck.status = 'IS';
  await truck.save();
  return res.status(200).json({message: 'Truck assigned successfully'});
});

module.exports = router;
