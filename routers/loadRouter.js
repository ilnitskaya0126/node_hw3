const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const { Load } = require('../schema/loadSchema.js');
const { User } = require('../schema/userSchema.js');
const { Truck } = require('../schema/truckSchema.js');


router.get('/', authMiddleware, async (req, res) => {

  const user = req.user;
  let loadsSort;

  if (user.role == "SHIPPER") {
    loadsSort = await Load.find({created_by: user._id});
  } else {
    loadsSort = await Load.find({assigned_to: user._id});
  }

  return res.status(200).send({loads: loadsSort });

});

router.post('/', authMiddleware, async (req, res) => {

  const user = req.user;

  if (user.role !== "SHIPPER") {
    return res.status(403).json({ message: 'Not available for this role' });
  }
  
  const load = new Load({
    ...req.body,
    created_by:req.user._id,
  });

  try {
    await load.save();
    return res.status(200).json({ message: "Load created successfully" });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }

});

router.get('/active', authMiddleware, async (req, res) => {

  const user = req.user;

  if (user.role !== "DRIVER") {
    return res.status(403).json({ message: 'Not available for this role' });
  }

  const load = await Load.findOne({assigned_to: user._id, status: 'ASSIGNED'}) || {};
  console.log(load)

  // if (!load) {
  //   return res.status(400).json({ message: 'No such load' });
  // }

  return res.status(200).send({load:load});

});

router.patch('/active/state', authMiddleware, async (req, res) => {

  const user = req.user;

  if (user.role !== "DRIVER") {
    return res.status(403).json({ message: 'Not available for this role' });
  }

  const states = Load.schema.path('state').enumValues;
  const load = Load.findOne({assigned_to: user._id});

  // if (!load.state) {
  //   return res.status(400).json({ message: 'No such active load' });
  // }

  // states.forEach((elem,id,arr) => {
  //   if(load.state == elem) {
  //     load.state = arr[id+1];
  //   }
  // })

  // if (load.state == states[3]) {

  // }

  if (load.state == states[0]) {
    load.state = states[1];
  } else if (load.state == states[1]) {
    load.state = states[2];
  } else if (load.state == states[2]) {
    load.state = states[3];
    load.status = 'SHIPPED';
  } 

  await load.save();
  res.status(200).json({ message: 'Success' });

});


router.get('/:id', authMiddleware, async (req, res) => {

  let _id = req.params.id;
  const load = await Load.findOne({ _id});

  // if (!load) {
  //   return res.status(400).json({ message: 'No such load' });
  // }

  return res.status(200).send({loads:load});

});


router.put('/:id', authMiddleware, async (req, res) => {

  const user = req.user;

  if (user.role !== "SHIPPER") {
    return res.status(403).json({ message: 'Not available for this role' });
  }

  let _id = req.params.id;
  let load = await Load.findOneAndUpdate({_id}, req.body, {new: true,upsert: true });

  if (!load) {
    return res.status(400).json({ message: 'No such note' });
  }

  return res.status(200).json({ message: 'Success' });

});

router.delete('/:id', authMiddleware, async (req, res) => {

  const user = req.user;

  if (user.role !== "SHIPPER") {
    return res.status(403).json({ message: 'Not available for this role' });
  }


  let _id = req.params.id;

  const load = await Load.findOne({ _id});

  if (!load) {
    return res.status(400).json({ message: 'No such load' });
  }

  await Load.findOneAndDelete({_id});
  return res.status(200).json({ message: 'Success' });

});

router.post('/:id/post', authMiddleware, async (req, res) => {
  const user = req.user;
  let _id = req.params.id;

  if (user.role !== "SHIPPER") {
    return res.status(403).json({ message: 'Not available for this role' });
  }

  const load = await Load.findOne({_id});

  if (!load) {
    return res.status(400).json({ message: 'No such load' });
  }

  const truck = await Truck.findOne({status :'IS'});

  if (!truck) {
    return res.status(400).json({ message: 'Not available driver found' });
  }
  const states = Load.schema.path('state').enumValues;

  truck.status = 'OL';
  load.status = 'ASSIGNED';
  load.assigned_to = truck.created_by;
  load.state = states[0];

  await load.save();
  await truck.save();

  return res.status(200).json({ message: "Load posted successfully",driver_found: true });
});

router.get('/:id/shipping_info', authMiddleware, async (req, res) => {


});



module.exports = router;
